FROM node:12

RUN mkdir -p /opt/eduardo

WORKDIR /opt/eduardo

COPY . ./

RUN npm install

EXPOSE 4200
